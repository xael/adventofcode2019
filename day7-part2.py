from itertools import permutations

OPCODE_STOP = "99"
OPCODE_ADD = "01"
OPCODE_MULTIPLY = "02"
OPCODE_INPUT = "03"
OPCODE_OUTPUT = "04"
OPCODE_JUMP_IF_TRUE = "05"
OPCODE_JUMP_IF_FALSE = "06"
OPCODE_LESS_THAN = "07"
OPCODE_EQUALS = "08"

EXIT_CODE_END = "END"
EXIT_CODE_INPUT = "INPUT"
EXIT_CODE_OUTPUT = "OUTPUT"

class IntcodeComputer:
    def __init__(self, name, code):
        self.name = name
        self.code = list(code)
        self.pos = 0
        
    def _get_args(self):
        val1 = val2 = 0
        if len(str(self.code[self.pos])) > 2:
            value = str(self.code[self.pos])
            opcode = value[-2:]
            immediate_mode_arg1 = (value[-3:-2] == "1")
            immediate_mode_arg2 = (value[-4:-3] == "1")
            # immediate_mode_arg3 = (value[-5:-4] == "1")
        else:
            opcode = f'{self.code[self.pos]:02}'
            immediate_mode_arg1 = False
            immediate_mode_arg2 = False
            # immediate_mode_arg3 = False
    
        nb_args = 0
        if opcode in [
                OPCODE_OUTPUT,
        ]:
            nb_args = 1
    
        if opcode in [
                OPCODE_ADD,
                OPCODE_MULTIPLY,
                OPCODE_JUMP_IF_TRUE,
                OPCODE_JUMP_IF_FALSE,
                OPCODE_LESS_THAN,
                OPCODE_EQUALS,
        ]:
            nb_args = 2
            
        if nb_args >= 1:
            if immediate_mode_arg1:
                val1 = self.code[self.pos + 1]
            else:
                val1 = self.code[self.code[self.pos + 1]]
    
        if nb_args >= 2:
            if immediate_mode_arg2:
                val2 = self.code[self.pos + 2]
            else:
                val2 = self.code[self.code[self.pos + 2]]
            
        return (opcode, val1, val2)

    
    def run_program(self, input_data=None):
        while self.pos < len(self.code):
            opcode, val1, val2 = self._get_args()
            
            if opcode == OPCODE_STOP:
                # print(f"{self.name} exit {self.code[0]}")
                return (EXIT_CODE_END, self.code[0])
        
            if opcode == OPCODE_ADD:
                self.code[self.code[self.pos + 3]] = val1 + val2
                self.pos += 4
        
            elif opcode == OPCODE_MULTIPLY:
                self.code[self.code[self.pos + 3]] = val1 * val2
                self.pos += 4
        
            elif opcode == OPCODE_INPUT:
                if input_data is not None:
                    self.code[self.code[self.pos + 1]] = int(input_data)
                    # print(f"{self.name} input {input_data}")
                    input_data = None
                else:
                    # self.code[self.code[self.pos + 1]] = int(input("Waiting value:"))
                    return (EXIT_CODE_INPUT, self.name)
    
                self.pos += 2
            
            elif opcode == OPCODE_OUTPUT:
                self.pos += 2
                # print(f"{self.name} output {val1}")
                return (EXIT_CODE_OUTPUT, val1)
        
            elif opcode == OPCODE_JUMP_IF_TRUE:
                if val1 != 0:
                    self.pos = val2
                else:
                    self.pos += 3
                
            elif opcode == OPCODE_JUMP_IF_FALSE:
                if val1 == 0:
                    self.pos = val2
                else:
                    self.pos += 3
                
            elif opcode == OPCODE_LESS_THAN:
                if val1 < val2:
                    self.code[self.code[self.pos + 3]] = 1
                else:
                    self.code[self.code[self.pos + 3]] = 0
                    
                self.pos += 4
                
            elif opcode == OPCODE_EQUALS:
                if val1 == val2:
                    self.code[self.code[self.pos + 3]] = 1
                else:
                    self.code[self.code[self.pos + 3]] = 0
                    
                self.pos += 4
                
            else:
                raise ValueError(f'Unknown opcode {opcode} at pos {self.pos}')
            


def truster(sequence, code):
    current_code = list(code)
    previous_code = 0

    amplis = {
        "A": IntcodeComputer("A", code),
        "B": IntcodeComputer("B", code),
        "C": IntcodeComputer("C", code),
        "D": IntcodeComputer("D", code),
        "E": IntcodeComputer("E", code),
    }
    # print(sequence)
    outA = amplis["A"].run_program(sequence[0])
    outB = amplis["B"].run_program(sequence[1])
    outC = amplis["C"].run_program(sequence[2])
    outD = amplis["D"].run_program(sequence[3])
    outE = amplis["E"].run_program(sequence[4])

    outE = (EXIT_CODE_OUTPUT, 0)
    lastE = None
    while EXIT_CODE_END not in [outA[0], outB[0], outC[0], outD[0], outE[0]]:
        outA = amplis["A"].run_program(outE[1])
        outB = amplis["B"].run_program(outA[1])
        outC = amplis["C"].run_program(outB[1])
        outD = amplis["D"].run_program(outC[1])
        outE = amplis["E"].run_program(outD[1])
        if outE[0] == EXIT_CODE_OUTPUT:
            lastE = outE[1]
        
    return lastE
        

def find_best_sequence(code):
    optimum = 0
    best_sequence = None

    for sequence in permutations([9,8,7,6,5], 5):
        value = truster(sequence, code)
        if value > optimum:
            optimum = value
            best_sequence = sequence

    return (best_sequence, optimum)


out = truster((9,8,7,6,5), [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5])
# assert out[0] == (9,8,7,6,5)
assert out == 139629729

out = find_best_sequence([3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10])
assert out[0] == (9,7,8,5,6)
assert out[1] == 18216


code = [3,8,1001,8,10,8,105,1,0,0,21,42,51,76,93,110,191,272,353,434,99999,3,9,1002,9,2,9,1001,9,3,9,1002,9,3,9,1001,9,2,9,4,9,99,3,9,1002,9,3,9,4,9,99,3,9,1002,9,4,9,101,5,9,9,1002,9,3,9,1001,9,4,9,1002,9,5,9,4,9,99,3,9,1002,9,5,9,101,3,9,9,102,5,9,9,4,9,99,3,9,1002,9,5,9,101,5,9,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99]


out = find_best_sequence(code)
print(out)
