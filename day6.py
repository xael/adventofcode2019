class Node:
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.children = []

    def new_child(self, name):
        child = Node(name, self)
        self.children.append(child)
        return child

    def change_parent(self, parent):
        if self.parent:
            self.parent.remove_child(self)
            
        self.parent = parent
        self.parent.add_child(self)

    def remove_child(self, child):
        self.children.pop(child)

    def add_child(self, child):
        self.children.append(child)
        return child
    
    @property
    def orbit_rank(self):
        if self.parent:
            return self.parent.orbit_rank + 1

        return 0

    @property
    def ancestors(self):
        if self.parent:
            return self.parent.ancestors + ',' + self.name

        return self.name


def find_orbital_moves(nodes, start, stop):
    start_ancestors = nodes[start].ancestors.split(",")
    stop_ancestors = nodes[stop].ancestors.split(",")
    common_path = None
    max_path_length = max(
        len(start_ancestors),
        len(stop_ancestors)
    )
    for i in range(max_path_length):
        if start_ancestors[i] == stop_ancestors[i]:
            common_path = start_ancestors[i]
            continue
        else:
            break

    return abs(
        (nodes[start].orbit_rank - nodes[common_path].orbit_rank - 1) + (nodes[stop].orbit_rank - nodes[common_path].orbit_rank - 1)
    )


def create_nodes_from_map(orbit_map):
    nodes = {
        "COM": Node("COM", None),
    }
    for line in orbit_map.split():
        try:
            parent, child = [x.strip() for x in line.split(')')]
        except ValueError:
            print(f"Error readins {line}")
            continue
        
        if parent not in nodes:
            print(f"unknow parent {parent}")
            nodes[parent] = Node(parent, None)
            
            # raise ValueError(f"unknow parent {parent}")

        if child not in nodes:
            nodes[child] = nodes[parent].new_child(child)
        else:
            nodes[child].change_parent(nodes[parent])
            # nodes[child] = nodes[parent].(nodes[child])
            # raise ValueError(f"multiple parents for child {child} / {parent}")

    return nodes


nodes = create_nodes_from_map(
    """COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L"""
)

print(nodes["D"].orbit_rank)
assert nodes["D"].orbit_rank == 3
assert nodes["L"].orbit_rank == 7
assert nodes["COM"].orbit_rank == 0

nodes = create_nodes_from_map(open("day6.input", "r").read())

count = 0
for node in nodes:
    count += nodes[node].orbit_rank

print(f"total number of direct and indirect orbits {count}")


#### Part 2

nodes = create_nodes_from_map("""
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN
""")

print(nodes["SAN"].ancestors)
print(nodes["YOU"].ancestors)

orbital_moves = find_orbital_moves(nodes, "SAN", "YOU")
print(f"Orbital moves : {orbital_moves}")


assert orbital_moves == 4


nodes = create_nodes_from_map(open("day6.input", "r").read())
orbital_moves = find_orbital_moves(nodes, "SAN", "YOU")
print(f"Orbital moves : {orbital_moves}")
