
def check_rules(value, test_range=True):
    if len(value) != 6:
        return False
    if not (134792 <= int(value) <= 675810) and test_range:
        return False
    if not (int(value[0]) <= int(value[1]) <= int(value[2]) <= int(value[3]) <= int(value[4]) <= int(value[5])):
        return False

    adjacent = False
    for i in range(5):
        if value[i] == value[i + 1]:
            adjacent = True
            break

    return adjacent
    

assert check_rules("111111", test_range=False) is True
assert check_rules("223450", test_range=False) is False
assert check_rules("123789", test_range=False) is False


count = 0
for i in range(134792, 675811):
    if check_rules(str(i)):
        count += 1

print(f"nombre: {count}")
