import pprint

class SpaceImageFormat:
    def __init__(self, data, x, y):
        self.data = data
        self.x = x
        self.y = y
        self.layers = {}

    @property
    def nb_layers(self):
        nb = int(len(self.data) / (self.x * self.y))
        return nb
        
    def fill(self):
        pos = 0
        for i in range(self.nb_layers):
            self.layers[i] = {}
            for iy in range(self.y):
                for ix in range(self.x):
                    try:
                        self.layers[i][iy][ix] = self.data[pos]
                    except KeyError:
                        self.layers[i][iy] = {ix: self.data[pos]}
                        
                    pos += 1

        if len(self.data) != pos:
            raise ValueError(f"len: {len(self.data)} / {pos}")

    def checksum(self):
        min_zero = None
        best_layer = None
        for i in range(self.nb_layers):
            zeros = 0
            for y in self.layers[i]:
                for x in self.layers[i][y]:
                    if self.layers[i][y][x] == "0":
                        zeros += 1

            if min_zero is None or min_zero > zeros:
                best_layer = i
                min_zero = zeros

        return best_layer
    
    def get_value(self, layer):
        nb_1 = 0
        nb_2 = 0
        for y in self.layers[layer]:
            for x in self.layers[layer][y]:
                if self.layers[layer][y][x] == "1":
                    nb_1 += 1
                if self.layers[layer][y][x] == "2":
                    nb_2 += 1

        return nb_1 * nb_2


# spi1 = SpaceImageFormat("123456789012", 3, 2)
# spi1.fill()

# pprint.pprint(spi1.layers)

data = open("day8.input", "r").read()

spi = SpaceImageFormat(data, 25, 6)
spi.fill()
# pprint.pprint(spi.layers)

layer = spi.checksum()

nb = spi.get_value(layer)
print(nb)
