from itertools import permutations

OPCODE_STOP = "99"
OPCODE_ADD = "01"
OPCODE_MULTIPLY = "02"
OPCODE_INPUT = "03"
OPCODE_OUTPUT = "04"
OPCODE_JUMP_IF_TRUE = "05"
OPCODE_JUMP_IF_FALSE = "06"
OPCODE_LESS_THAN = "07"
OPCODE_EQUALS = "08"

def get_args(code, pos):
    val1 = val2 = 0
    if len(str(code[pos])) > 2:
        value = str(code[pos])
        opcode = value[-2:]
        immediate_mode_arg1 = (value[-3:-2] == "1")
        immediate_mode_arg2 = (value[-4:-3] == "1")
        # immediate_mode_arg3 = (value[-5:-4] == "1")
    else:
        opcode = f'{code[pos]:02}'
        immediate_mode_arg1 = False
        immediate_mode_arg2 = False
        # immediate_mode_arg3 = False

    nb_args = 0
    if opcode in [
            OPCODE_OUTPUT,
    ]:
        nb_args = 1

    if opcode in [
            OPCODE_ADD,
            OPCODE_MULTIPLY,
            OPCODE_JUMP_IF_TRUE,
            OPCODE_JUMP_IF_FALSE,
            OPCODE_LESS_THAN,
            OPCODE_EQUALS,
    ]:
        nb_args = 2
        
    if nb_args >= 1:
        if immediate_mode_arg1:
            val1 = code[pos + 1]
        else:
            val1 = code[code[pos + 1]]

    if nb_args >= 2:
        if immediate_mode_arg2:
            val2 = code[pos + 2]
        else:
            val2 = code[code[pos + 2]]
        
    return (opcode, val1, val2)


def run_program(code, input_data=[]):
    pos = 0
    output = []
    while pos < len(code):
        opcode, val1, val2 = get_args(code, pos)
        
        if opcode == OPCODE_STOP:
            return (code[0], output)
    
        if opcode == OPCODE_ADD:
            code[code[pos + 3]] = val1 + val2
            pos += 4
    
        elif opcode == OPCODE_MULTIPLY:
            code[code[pos + 3]] = val1 * val2
            pos += 4
    
        elif opcode == OPCODE_INPUT:
            if len(input_data)  > 0:
                code[code[pos + 1]] = int(input_data.pop(0))
            else:
                code[code[pos + 1]] = int(input("Waiting value:"))

            pos += 2
        
        elif opcode == OPCODE_OUTPUT:
            output.append(val1)
            pos += 2
    
        elif opcode == OPCODE_JUMP_IF_TRUE:
            if val1 != 0:
                pos = val2
            else:
                pos += 3
            
        elif opcode == OPCODE_JUMP_IF_FALSE:
            if val1 == 0:
                pos = val2
            else:
                pos += 3
            
        elif opcode == OPCODE_LESS_THAN:
            if val1 < val2:
                code[code[pos + 3]] = 1
            else:
                code[code[pos + 3]] = 0
                
            pos += 4
            
        elif opcode == OPCODE_EQUALS:
            if val1 == val2:
                code[code[pos + 3]] = 1
            else:
                code[code[pos + 3]] = 0
                
            pos += 4
            
        else:
            raise ValueError(f'Unknown opcode {opcode} at pos {pos}')
        


def truster(sequence, code):
    current_code = list(code)
    previous_code = 0
    for phase in sequence:
        output = run_program(code=current_code, input_data=[phase, previous_code])
        previous_code = output[1][0]

    return previous_code
        

def find_best_sequence(code):
    optimum = 0
    best_sequence = None
    for sequence in permutations([4,3,2,1,0], 5):
        value = truster(sequence, code)
        if value > optimum:
            optimum = value
            best_sequence = sequence

    return (best_sequence, optimum)


out = find_best_sequence([3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0])
assert out[0] == (4, 3, 2, 1, 0)
assert out[1] == 43210

out = find_best_sequence([3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0])
assert out[0] == (0, 1, 2, 3, 4)
assert out[1] == 54321

out = find_best_sequence([3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0])
assert out[0] ==  (1, 0, 4, 3, 2)
assert out[1] == 65210


code = [3,8,1001,8,10,8,105,1,0,0,21,42,51,76,93,110,191,272,353,434,99999,3,9,1002,9,2,9,1001,9,3,9,1002,9,3,9,1001,9,2,9,4,9,99,3,9,1002,9,3,9,4,9,99,3,9,1002,9,4,9,101,5,9,9,1002,9,3,9,1001,9,4,9,1002,9,5,9,4,9,99,3,9,1002,9,5,9,101,3,9,9,102,5,9,9,4,9,99,3,9,1002,9,5,9,101,5,9,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99]


out = find_best_sequence(code)
print(out)
