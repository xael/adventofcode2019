program = [
    1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 13, 1, 19, 1, 19, 10, 23, 1, 23, 13, 27, 1, 6, 27, 31, 1, 9, 31,
    35, 2, 10, 35, 39,
    1, 39, 6, 43, 1, 6, 43, 47, 2, 13, 47, 51, 1, 51, 6, 55, 2, 6, 55, 59, 2, 59, 6, 63, 2, 63, 13, 67, 1, 5, 67, 71, 2,
    9, 71, 75, 1, 5, 75, 79, 1, 5, 79, 83, 1, 83, 6, 87, 1, 87, 6, 91, 1, 91, 5, 95, 2, 10, 95, 99, 1, 5, 99, 103, 1,
    10, 103, 107,
    1, 107, 9, 111, 2, 111, 10, 115, 1, 115, 9, 119, 1, 13, 119, 123, 1, 123, 9, 127, 1, 5, 127, 131, 2, 13, 131, 135,
    1, 9, 135, 139,
    1, 2, 139, 143, 1, 13, 143, 0, 99, 2, 0, 14, 0,
]


program[1] = 12
program[2] = 2


def run_program(code, pos=0):
    if code[pos] == 99:
        # print(" STOP")
        return code

    if code[pos] == 1:
        # print(f" ADD {code[pos + 1]} and {code[pos + 2]} => {code[pos + 3]}")
        code[code[pos + 3]] = code[code[pos + 1]] + code[code[pos + 2]]
        pos += 4
        return run_program(list(code), pos)

    if code[pos] == 2:
        # print(f" MULT {code[pos + 1]} and {code[pos + 2]} => {code[pos + 3]}")
        code[code[pos + 3]] = code[code[pos + 1]] * code[code[pos + 2]]
        pos += 4
        return run_program(list(code), pos)


assert run_program([1, 0, 0, 0, 99]) == [2, 0, 0, 0, 99]
assert run_program([2, 3, 0, 3, 99]) == [2, 3, 0, 6, 99]
assert run_program([2, 4, 4, 5, 99, 0]) == [2, 4, 4, 5, 99, 9801]
assert run_program([1, 1, 1, 4, 99, 5, 6, 0, 99]) == [30, 1, 1, 4, 2, 5, 6, 0, 99]

working_program = list(program)
result = run_program(working_program)

print(f"Working_Program: {result[0]}")

print('-----------------------------')


def program_for(noun, verb, program):
    test = list(program)
    test[1] = noun
    test[2] = verb
    return run_program(test)


def display_result(noun, verb):
    return 100 * noun + verb


assert display_result(12, 2) == 1202

found = False
noun = 0
while not found and noun < 100:
    verb = 0
    while not found and verb < 100:
        result = program_for(noun, verb, program)
        if result and result[0] == 19690720:
            print(f"Noun: {noun}, Verb: {verb}, result: {display_result(noun, verb)} ")
            found = True

        verb += 1

    noun += 1
