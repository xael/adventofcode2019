# Day 1

# Part 1


def fuel_for_module(module):
    fuel = int(module / 3) - 2
    if fuel > 0:
        return fuel

    return 0


assert fuel_for_module(12) == 2
assert fuel_for_module(14) == 2
assert fuel_for_module(1969) == 654
assert fuel_for_module(100756) == 33583

modules = [
    102777, 107296, 131207, 116508, 99009, 120098, 83121, 87846, 126604,
    79906, 63668, 143932, 51829, 106383, 121354, 138556, 123426, 111544, 84395,
    147066, 61897, 133724, 75867, 106697, 67782, 86191, 50666, 138928, 118740,
    136863, 123108, 85168, 138487, 115656, 104811, 114986, 147241, 73860, 99186,
    134657, 98379, 59914, 144863, 119851, 82549, 93564, 79437, 70761, 134303,
    108109, 116208, 80702, 111018, 131996, 119367, 74305, 65905, 116871, 102184,
    101880, 100453, 111281, 103134, 129529, 133885, 76153, 56890, 86262, 52804,
    139907, 131360, 80009, 121015, 74438, 54470, 73386, 112961, 116283, 81353,
    80610, 142522, 64946, 125652, 61688, 58367, 118930, 89711, 115239, 66403,
    92405, 114593, 112818, 75964, 126093, 139781, 144801, 88725, 125958, 116869,
    119676,
]


def fuel_for_modules(modules):
    sum_fuel_for_modules = 0
    for module in modules:
        sum_fuel_for_modules += fuel_for_module(module)

    return sum_fuel_for_modules


print("Day1 fuel for modules only:", fuel_for_modules(modules))

# Part 2
assert fuel_for_modules(modules) == 3429947


def fuel_for_modules_and_fuel(modules):
    sum_fuel_for_modules = 0
    fuel_for_fuel = 0
    for module in modules:
        sum_fuel_for_modules += fuel_for_module(module)
        
        f = fuel_for_module(module)
        while fuel_for_module(f) != 0:
            fuel_for_fuel += fuel_for_module(f)
            f = fuel_for_module(f)

    return fuel_for_fuel + sum_fuel_for_modules


assert fuel_for_modules_and_fuel([14]) == 2
assert fuel_for_modules_and_fuel([1969]) == 966
assert fuel_for_modules_and_fuel([100756]) == 50346

print("Day1 fuel for modules including fuel for fuel:", fuel_for_modules_and_fuel(modules))
