import pprint

class SpaceImageFormat:
    def __init__(self, data, x, y):
        self.data = data
        self.x = x
        self.y = y
        self.layers = {}

    @property
    def nb_layers(self):
        nb = int(len(self.data) / (self.x * self.y))
        return nb
        
    def fill(self):
        pos = 0
        for i in range(self.nb_layers):
            for iy in range(self.y):
                for ix in range(self.x):
                    if iy not in self.layers:
                        self.layers[iy] = {ix: self.data[pos]}
                    else:
                        if ix not in self.layers[iy]:
                            self.layers[iy][ix] = self.data[pos]
                        else:
                            if self.layers[iy][ix] == "2":
                                self.layers[iy][ix] = self.data[pos]
                        
                    pos += 1

        if len(self.data) != pos:
            raise ValueError(f"len: {len(self.data)} / {pos}")

    def print(self):
        text = ""
        for y in range(self.y):
            for x in range(self.x):
                text += self.layers[y][x]

            text += '\n'

        return text.replace('0', ' ')
    
# spi1 = SpaceImageFormat("123456789012", 3, 2)
# spi1.fill()

# pprint.pprint(spi1.layers)

data = open("day8.input", "r").read()

spi = SpaceImageFormat(data, 25, 6)
spi.fill()
pprint.pprint(spi.layers)
print(spi.print())
