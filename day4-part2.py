
def check_rules(value, test_range=True):
    if len(value) != 6:
        return False
    if not (134792 <= int(value) <= 675810) and test_range:
        return False
    if not (int(value[0]) <= int(value[1]) <= int(value[2]) <= int(value[3]) <= int(value[4]) <= int(value[5])):
        return False

    adjacent = False
    value = "-" + value + "-"
    for i in range(1, 6):
        if value[i] == value[i + 1] and value[i] != value[i + 2] and value[i] != value[i - 1]:
            adjacent = True
            break

    return adjacent
    

assert check_rules("112233", test_range=False) is True
assert check_rules("123444", test_range=False) is False
assert check_rules("111122", test_range=False) is True


count = 0
for i in range(134792, 675811):
    if check_rules(str(i)):
        count += 1

print(f"nombre: {count}")
